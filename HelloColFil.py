# -*- coding:utf-8 -*-
'''
http://sinhrks.hatenablog.com/entry/2015/04/26/200631
http://aima.cs.berkeley.edu/data/iris.csv
http://qiita.com/t-yotsu/items/4cabd1ae5406cfd7d741
https://github.com/apache/spark/blob/master/python/pyspark/mllib/recommendation.py
http://qiita.com/kenmatsu4/items/42fa2f17865f7914688d
http://qiita.com/junkls/items/10384950963056cc8e08
'''

import pandas as pd
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
import itertools
from collections import OrderedDict


def ReadCSV(name, sc):
  # 表示する行数を設定
  pd.options.display.max_rows=14

  # 列名の設定
  names = ['userID', 'productID', 'eval']

  # 列の値の型を設定
  dtype = {'userID':'int64','productID':'int64', 'Eval':'int64'}

  # pandasを用いてCSVを読み込む
  pdf = pd.read_csv(name, header=None, names=names, dtype=dtype)
  #pdf['Eval'] = pdf['Eval'].convert_objects(convert_numeric=True)
  print(pdf)

  # Sparkの各種設定
  #conf = SparkConf().setMaster("local").setAppName("Pandas PySpark App")
  #sc = SparkContext(conf = conf)
  sqlContext = SQLContext(sc)

  # pandasのDataFrameからSparkのDataFrameに変換
  sdf = sqlContext.createDataFrame(pdf)
  # print(sdf.take(100))
  return sdf

# Sparkの各種設定
conf = SparkConf().setMaster("local").setAppName("Pandas PySpark App")
sc = SparkContext(conf = conf)

sdf = ReadCSV('./data.csv',sc)

'''
f = open('./input.txt', 'w')
for sddd in sdf:
  for jj in xrange(1, 13):
    f.write('(userID = ' + str(int(sddd[0][jj])) + ', productID = ' + str(int(sddd[1][jj])) + ') = ' + str(float(sddd[2][jj])) + '\n')
  #f.write(str(sdd[0]))
f.close()
'''

# (userID, productID), Eval の形式に変換
ratings = sdf.map(lambda l: Rating(int(l[0]), int(l[1]), float(l[2])))
# print(ratings.take(100))
# print("\n")

# 最小交差二乗 (ALS) を用いたレコメンデーションモデルの構築 
rank = 10
numIterations = 10
model = ALS.train(ratings, rank, numIterations)

# レコメンデーションモデルを構築するのに用いたデータに対する評価値の推測を実施 
testdata = ratings.map(lambda p: (p[0], p[1]))
predictions = model.predictAll(testdata).map(lambda r: ((r[0], r[1]), r[2]))
ratesAndPreds = ratings.map(lambda r: ((r[0], r[1]), r[2])).join(predictions)
# print(ratesAndPreds.take(100))
# print("\n")

# 生成したレコメンデーションモデルの精度をMSEで確認
MSE = ratesAndPreds.map(lambda r: (r[1][0] - r[1][1])**2).mean()
print("Mean Squared Error = " + str(MSE))

maxUserID = 10
maxProductID = 10
f = open('./predict.csv','w')
# 生成したレコメンデーションモデルで欠損データの推測
for ii in xrange(1, maxUserID):
  for jj in xrange(1, maxProductID):
    f.write(str(ii) + ',' + str(jj) + ',' + str(model.predict(ii,jj)) + '\n')
    # print('(userID = ' + str(ii) + ', productID = ' + str(jj) + ') = ' + str(model.predict(ii, jj)) + '\n')
f.close()

f = open('./recommend.txt', 'w')
for ii in xrange(1, maxUserID):
  maxID=1
  max = model.predict(ii, 1)

  for jj in xrange(1, maxProductID):
    if(max >= model.predict(ii, jj)):
      maxID = jj
      max = model.predict(ii, jj)

  print('userID = ' + str(ii) + ' -> recommend_productID = ' + str(maxID) + ' (predict_Eval = ' + str(model.predict(ii, maxID)) + ')\n')
  f.write('userID = ' + str(ii) + ' -> recommend_productID = ' + str(maxID) + ' (predict_Eval ' + str(model.predict(ii, maxID)) + ')\n')
#sdfp = ReadCSV('./predict.csv', sc)

#sdfp.filter(sdfp['userID'] == 1).show()
